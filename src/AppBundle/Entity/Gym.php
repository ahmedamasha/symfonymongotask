<?php
 namespace AppBundle\Entity;
 use Symfony\Component\Validator\Constraints as Assert;

class Gym
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $ownerEmail;
    public function getEmail()
    {
        return $this->ownerEmail;
    }

    public function setEmail($ownerEmail)
    {
         $this->ownerEmail=$ownerEmail   ;
        return $this;
    }


}