<?php

 namespace AppBundle\Controller;
 use AppBundle\Entity\Gym;
 use MongoClient;
 use Symfony\Component\HttpFoundation\JsonResponse;
 use Symfony\Component\HttpFoundation\Response;
 use Symfony\Bundle\FrameworkBundle\Controller\Controller;
 use Symfony\Component\HttpFoundation\Request;
 use Acme\StoreBundle\Document\Products;
 use Symfony\Component\Validator\Constraints\Collection;
 use Symfony\Component\Validator\Constraints\Email;
 use Symfony\Component\Validator\Constraints\Date;
 use Symfony\Component\Validator\Constraints\NotBlank;
class GymController extends Controller
{

    public function createAction(Request $request)
   {

       $user = new Gym();
        // ... do something to the $author object
       $user->setEmail($request->get('ownerEmail'));
       $returnArray=array();


       $validator = $this->get('validator');
       $errors = $validator->validate($user);

       foreach ($errors as $error){
           $returnArray['errors'][]=array('field'=>$error->getPropertyPath(),'message'=>$error->getMessage());
           //$returnArray['errors'][$error->getPropertyPath()]=$error->getMessage();
       }

       if (count($errors) > 0) {
           $returnArray['status']="fail";
       } else {

           $doc = array(
               "name" => $request->get('name'),
               "longitude" => $request->get('longitude'),
               "latitude" => $request->get('latitude'),
               "ownerEmail" => $request->get('ownerEmail')
           );

           $connection = new MongoClient();
           $collection = $connection->gymentry->products;

            $collection->insert( $doc );
           $returnArray['status']="success";

       }

       return new JsonResponse($returnArray);


    }


    public function showAction()
    {
        $repository = $this->get('doctrine_mongodb')
            ->getManager()
            ->getRepository('AcmeStoreBundle:Products');
        $products = $repository->findAll();

        $response = new Response(json_encode($products));
        $response->headers->set('Content-Type', 'application/json');
        return  $response;
    }

}